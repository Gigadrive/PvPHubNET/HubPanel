<pre id="clog" style="overflow: auto; height:400px;">
<?php
if(!isset($_GET['dir']) or !isset($_GET['type'])) {
?>
Required Arguments not set!
<?php
}
else if(__FILE__ == $_SERVER['DOCUMENT_ROOT'].$_SERVER['PHP_SELF']) {
?>
Direct Console opening is not allowed!
<?php
}
else {
	$type = $_GET['type'];
	$directory = $_GET['dir'];
	if($type == "proxy") {
		$file = file($directory . "proxy.log.0");

		for ($i = count($file)-101; $i < count($file); $i++) {
			if($i < 0) {
				continue;
			}
			echo $file[$i];
		}
	}
	else if($type == "server") {
		$file = file($directory . "logs/latest.log");

		for ($i = count($file)-101; $i < count($file); $i++) {
			if($i < 0) {
				continue;
			}
			echo $file[$i];
		}
	}
	else {
?>
Unknown Type! Please Change the Database Entry
<?php
	}
}
?>
</pre>
