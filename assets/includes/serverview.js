function scrollDown(pre) {
	var clog = document.getElementById(pre);
	clog.scrollTop = clog.scrollHeight;
}
function updateConsole(pre, div, directory, type, button) {
	bobj = $(button);
	bobj.button('loading');
	if (window.XMLHttpRequest) {
		xmlhttp=new XMLHttpRequest();
	}

	else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET","/assets/includes/console.php?dir=" + directory + "&type=" + type,false);
	xmlhttp.send(null);
	document.getElementById(div).innerHTML=xmlhttp.responseText;

	scrollDown(pre);
	setTimeout(
		function() {
			scrollDown(pre);
			bobj.button('reset');
		}
	, 100);
}
