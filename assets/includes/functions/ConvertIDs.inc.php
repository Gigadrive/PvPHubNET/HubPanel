<?php
class CL_ConvertIDs {
	public static function getRankName($rankID){
		if($rankID == 0){
			return "Spieler";
		} else if($rankID == 1){
			return "Premium";
		} else if($rankID == 2){
			return "Life-Time-Premium";
		} else if($rankID == 3){
			return "VIP";
		} else if($rankID == 4){
			return "YouTuber";
		} else if($rankID == 5){
			return "Beta";
		} else if($rankID == 6){
			return "Bau-Team";
		} else if($rankID == 7){
			return "Server-Team";
		} else if($rankID == 8){
			return "Moderator";
		} else if($rankID == 9){
			return "Developer";
		} else if($rankID == 10){
			return "Administrator";
		} else if($rankID == 11){
			return "Owner";
		} else {
			return $rankID;
		} 
	}
	
	public static function getLanguageName($langID){
		if($langID == 0){
			return "German";
		} elseif($langID == 1){
			return "English";
		} else {
			return $langID;
		}
	}
	
	public static function getRankLink($rankID){
		if($rankID == 0){
			return "userLink";
		} elseif($rankID == 1){
			return "premLink";
		} elseif($rankID == 2){
			return "premLink";
		} elseif($rankID == 3){
			return "ytLink";
		} elseif($rankID == 4){
			return "ytLink";
		} elseif($rankID == 5){
			return "betaLink";
		} elseif($rankID == 6){
			return "buildLink";
		} elseif($rankID == 7){
			return "teamLink";
		} elseif($rankID == 8){
			return "modLink";
		} elseif($rankID == 9){
			return "devLink";
		} elseif($rankID == 10){
			return "adminLink";
		} elseif($rankID == 11){
			return "ownerLink";
		} else {
			return $rankID;
		}
	}

    public static function getLogPrefix($prefixID){
        if($prefixID == "common"){
            return "Allgemein";
        } else if($prefixID == "website"){
            return "Website";
        } else if($prefixID == "ts3"){
            return "TeamSpeak3-Server";
        } else if($prefixID == "minecraft"){
            return "Minecraft-Server";
        } else if($prefixID == "hg"){
            return "Hunger Games";
        } else if($prefixID == "infw"){
            return "Infection Wars";
        } else if($prefixID == "dm"){
            return "Deathmatch";
        } else if($prefixID == "spvp"){
            return "SoupPvP";
        } else {
            return $prefixID;
        }
    }
    
    public static function getArticleCat($catID){
        if($catID == "common"){
            return "Allgemeine Info";
        } else if($catID == "trailer"){
            return "Trailer / Teaser";
        } else if($catID == "livestream"){
            return "Livestream";
        } else if($catID == "maintenance"){
            return "Wartungsarbeiten";
        } else {
            return $catID;
        }
    }
    
    public static function getReportCategory($catID){
        if($catID == "account"){
            return "Account-Verwaltung";
        } else if($catID == "bug_ts3"){
            return "TeamSpeak3-Bug";
        } else if($catID == "bug_mc"){
            return "Minecraft-Bug";
        } else if($catID == "bug_web"){
            return "Website-Bug";
        } else if($catID == "premium"){
            return "Premium-Kauf";
        } else if($catID == "ban"){
            return "Entbannungsantrag";
        } else {
            return $catID;
        }
    }
}
?>
