<?php session_start();
//error_reporting(E_ALL);
//ini_set("display_errors", "on");
//ini_set("display_startup_errors", "on");
//if(!isset($nosql)) {
	require_once("/var/www/html/admin/assets/includes/mysql.inc.php");
//}
require_once("/var/www/html/admin/assets/includes/globalFunctions.inc.php");

if(!isset($_SESSION["username"])) {
	header("Location: /");
}

if($_SESSION["rank"] < 8){
	header("Location: /logout");
}

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php
	
		// Page Titles
		$page = "";
		
		$dashboard = false;
		$servers = false;
		$proxies = false;
		$stats = false;
		$players = false;
		
		if($_SERVER["PHP_SELF"] == "/dashboard/index.php"){
			$dashboard = true;
			$page = "Dashboard"; 
		} else if($_SERVER["PHP_SELF"] == "/servers/index.php"){
			$servers = true;
			$page = "Server"; 
		} else if($_SERVER["PHP_SELF"] == "/proxies/index.php"){
			$proxies = true;
			$page = "Proxies"; 
		} else if($_SERVER["PHP_SELF"] == "/stats/index.php"){
			$stats = true;
			$page = "Statistiken"; 
		} else if($_SERVER["PHP_SELF"] == "/players/index.php"){
			$players = true;
			$page = "Spieler"; 
		} else if($_SERVER["PHP_SELF"] == "/players/view/index.php"){
			$players = true;
			$page = $_GET["user"];
		} else if($_SERVER["PHP_SELF"] == "/players/edit/index.php"){
			$players = true;
			$page = $_GET["user"];
		} else if($_SERVER["PHP_SELF"] == "/players/staff/index.php"){
			$players = true;
			$page = "Team-Mitglieder";
		} else if($_SERVER["PHP_SELF"] == "/players/beta/index.php"){
			$players = true;
			$page = "Beta-Tester";
		} else if($_SERVER["PHP_SELF"] == "/players/vip/index.php"){
			$players = true;
			$page = "VIPs";
		} else if($_SERVER["PHP_SELF"] == "/players/senior/index.php"){
			$players = true;
			$page = "Senior-Staff-Members";
		} else {
			$page = "Dashboard";
		}
	
	?>

    <title><?php print $page; ?> - HubPanel</title>

    <!-- Core CSS - Include with every page -->
    <link href="/admin/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/admin/assets/css/hubpanel.css" rel="stylesheet">
    <link href="/admin/assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="/admin/assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="/admin/assets/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="/admin/assets/css/sb-admin.css" rel="stylesheet">
    
    <!-- TinyMCE Init -->
    <script src="/admin/assets/js/tinymce/tinymce.min.js"></script>
    <script>
        tinymce.init({selector:'textarea'});
    </script>


    <meta name="robots" content="noindex, nofollow"/>

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a style="font-weight: bolder" class="navbar-brand" href="/admin"><i class="fa fa-th-large"></i> HubPanel</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <?php print $_SESSION["username"]; ?>  <span class="badge"><?php print CL_ConvertIDs::getRankName($_SESSION["rank"]); ?></span>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="/admin/players/view/?user=<?php print $_SESSION["username"]; ?>"><i class="fa fa-user fa-fw"></i> Profil</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <form action="/admin/player-search/index.php" method="post"><div class="input-group custom-search-form">
                                <input type="text" name="ign" class="form-control" placeholder="Spieler-Suche..">
                                <span class="input-group-btn">
                                <button type="submit" name="submit" class="btn btn-default">
                                	<i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            </form>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="/admin/dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Spieler<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/players">&Uuml;bersicht</a>
                                </li>
                                <li>
                                    <a href="#">R&auml;nge <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="/admin/players/staff">Team-Mitglieder</a>
                                        </li>
                                        <li>
                                            <a href="/admin/players/beta">Beta-User</a>
                                        </li>
                                        <li>
                                            <a href="/admin/players/vip">YouTuber &amp; VIP</a>
                                        </li>
                                        <li>
                                            <a href="/admin/players/beta-keys">Beta-Key-Management</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <li>
                                    <a href="/admin/players/bans">Bans</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-question fa-fw"></i> Moderation<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/admin/moderation">&Uuml;bersicht</a>
                                </li>
                                <li>
                                    <a href="/admin/moderation/nicks">/nick Logger</a>
                                </li>
                                <li>
                                    <a href="/admin/moderation/cmdBlock">Commandblock Logger</a>
                                </li>
                                <li>
                                    <a href="/admin/moderation/team-apply">Team-Bewerbungen</a>
                                </li>
                                <li>
                                    <a href="#">Tickets <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="/admin/moderation/tickets">&Uuml;bersicht</a>
                                        </li>
                                        <li>
                                            <a href="/admin/moderation/tickets/account">Account-Verwaltung</a>
                                        </li>
                                        <li>
                                            <a href="/admin/moderation/tickets/reports">User Meldungen</a>
                                        </li>
                                        <li>
                                            <a href="/admin/moderation/tickets/bugs-ts3">Bugs (TeamSpeak)</a>
                                        </li>
                                        <li>
                                            <a href="/admin/moderation/tickets/bugs-www">Bugs (Wesbite)</a>
                                        </li>
                                        <li>
                                            <a href="/admin/moderation/tickets/bugs-mc">Bugs (Minecraft)</a>
                                        </li>
                                        <li>
                                            <a href="/admin/moderation/tickets/premium">Premium-K&auml;ufe</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-briefcase fa-fw"></i> Content Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">News <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="/admin/cms/news/">&Uuml;bersicht</a>
                                        </li>
                                        <li>
                                            <a href="/admin/cms/news/new/">Neuen Artikel schreiben</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <li>
                                    <a href="#">Achievements <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="/admin/cms/news/">&Uuml;bersicht</a>
                                        </li>
                                        <li>
                                            <a href="/admin/cms/news/new/">Neues Achievement</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
		<div id="page-wrapper">
