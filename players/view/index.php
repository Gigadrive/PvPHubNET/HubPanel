<?php
if(isset($_GET["user"])){
	$u = $_GET["user"];
} else {
	header("Location: /admin/players");
}

require_once "/var/www/html/admin/assets/includes/mysql.inc.php";

$len = strlen($u);
if($len > 0 and $len < 17) {
	$sql = mysql_query("SELECT * FROM users WHERE name='" . $u . "'");
}
else if($len == 32) {
	$sql = mysql_query("SELECT * FROM users WHERE name='" . $u . "'");
}
else {
	header("Location: /admin/players");
}

if(mysql_num_rows($sql) != 1){
	header("Location: /admin/players");
}

	while($row = mysql_fetch_array($sql)){
		$uid = $row["id"];
		$lastname = $row["name"];
		$rankid = $row["rank"];
		$uuid = $row["uuid"];
		$friends = $row["friends"];
		$facebook = $row["fb"];
		$twitter = $row["tw"];
		$youtube = $row["yt"];
		$coins = $row["coins"];
		$chips = $row["chips"];
		$joined = $row["firstjoin"];
		$password = $row["website_pass"];
	}

	if(strlen($description) > 50) {
		$cdescr = substr($description, 0, 50) . "...";
	}
	else {
		$cdescr = $description;
	}

	if($email == null) {
		$email = "<i>Dieser Nutzer ist nicht registriert</i>";
		$fpw = "<i>Dieser Nutzer ist nicht registriert</i>";
	}
	else {
		$fpw = "<i>Kann aus Sicherheitsgr&uuml;nden nicht angezeigt werden</i>";
	}

	$nosql = true;
	require_once "/var/www/html/admin/assets/includes/top.inc.php";
?>
        
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php print $u; ?> <?php if(!$_SESSION["rank"] < 9){ ?>
                    	<a href="/admin/players/edit/?user=<?php print $u; ?>">
                    		<button class="btn btn-lg btn-primary" style="float: right">Bearbeiten</button>
                    	</a><br />
                    <?php } ?></h1>
                    <div class="row">
                    	<div class="col-md-3">
                    		<img src="https://a.vatar.co/a/<?php print $u; ?>/200.png" alt="<?php print $u; ?>"/>
                    	</div>
                    	<div class="col-md-9">
                    		<div class="panel panel-default">
                    			<div class="panel-heading">Account Daten</div>
                    			<div class="panel-body">
                    				<div class="table-responsive">
		                                <table class="table table-striped">
		                                    <tbody>
		                                        <tr>
		                                            <td><b>UID:</b></td>
		                                            <td><?php print $uid; ?></td>
		                                        </tr>
												<tr>
		                                            <td><b>UUID:</b></td>
		                                            <td><?php print $uuid; ?></td>
		                                        </tr>
		                                        <tr>
		                                            <td><b>Letzt bekannter Username:</b></td>
		                                            <td><?php print $lastname; ?></td>
		                                        </tr>
		                                        <tr>
		                                            <td><b>Rang:</b></td>
		                                            <td>(<?php print $rankid; ?>) <?php print CL_ConvertIDs::getRankName($rankid); ?></td>
		                                        </tr>
		                                        <tr>
		                                            <td><b>Coins:</b></td>
		                                            <td><?php print $coins; ?></td>
		                                        </tr>
		                                        <tr>
		                                            <td><b>Chips:</b></td>
		                                            <td><?php print $chips; ?></td>
		                                        </tr>
		                                    </tbody>
		                                </table>
		                            </div>
                    			</div>
                    		</div>

							<div class="panel panel-default">
                    			<div class="panel-heading">Nutzer Daten</div>
                    			<div class="panel-body">
                    				<div class="table-responsive">
		                                <table class="table table-striped">
		                                    <tbody>
												<tr>
		                                            <td><b>Erster Beitritt:</b></td>
		                                            <td><?php print $joined; ?></td>
		                                        </tr>
		                                        <tr>
		                                            <td><b>Freunde:</b></td>
		                                            <td><?php print $friends; ?></td>
		                                        </tr>
		                                    </tbody>
		                                </table>
		                            </div>
                    			</div>
                    		</div>

                    		<div class="panel panel-default">
                    			<div class="panel-heading">Kontakt Details</div>
                    			<div class="panel-body">
                    				<div class="table-responsive">
		                                <table class="table table-striped">
		                                    <tbody>
		                                        <tr>
		                                            <td><b>Facebook:</b></td>
		                                            <td><?php print $facebook; ?></td>
		                                        </tr>
		                                        <tr>
		                                            <td><b>Twitter:</b></td>
		                                            <td>@<?php print $twitter; ?></td>
		                                        </tr>
		                                        <tr>
		                                            <td><b>YouTube:</b></td>
		                                            <td><?php print $youtube; ?></td>
		                                        </tr>
		                                    </tbody>
		                                </table>
		                            </div>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                </div>
            </div>
<?php require_once "/var/www/html/admin/assets/includes/bottom.inc.php"; ?>
