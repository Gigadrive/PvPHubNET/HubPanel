<?php session_start();
error_reporting(E_ALL);
ini_set("display_errors", "on");
ini_set("display_startup_errors", "on");

include("/var/www/html/admin/assets/includes/mysql.inc.php");

if(!isset($_SESSION["username"])){
    header("Location: /");
}

function generateRandomString($length) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

if($_SESSION["rank"] > 8){

    $key = "" . generateRandomString(4) . "-" . generateRandomString(4) . "-" . generateRandomString(4) . "-" . generateRandomString(4);
    $sql = mysql_query("INSERT INTO beta_keys (`beta_key`) VALUES('" . $key . "');");
    header("Location: /admin/players/beta-keys");
}

?>