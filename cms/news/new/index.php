<?php require_once "/var/www/html/admin/assets/includes/top.inc.php"; ?>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Neuen News-Artikel ver&ouml;ffentlichen</h1>
                    <form action="/admin/assets/includes/forms/new_article.php" method="post">
                        <center>
                            <div class="input-group input-group-lg">
                                <input type="text" placeholder="Titel.." class="form-control" name="title"/>
                            </div>
                            <div class="input-group">
                                <textarea name="content">
                                    Text
                                </textarea>
                                <select name="category">
                                    <option value="common" selected>Allgemein</option>
                                    <option value="maintenance">Server-Wartung</option>
                                    <option value="livestream">Livestream</option>
                                    <option value="trailer">Trailer</option>
                                </select>
                                <input type="submit" name="submit" value="Speichern" class="btn btn-lg btn-primary" style="float: right"/>
                            </div>
                        </center>
                    </form>
                </div>
            </div>
<?php require_once "/var/www/html/admin/assets/includes/bottom.inc.php"; ?>
