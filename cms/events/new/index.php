<?php require_once "/var/www/html/admin/assets/includes/top.inc.php"; ?>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Neues Event planen</h1>
                    <form action="/admin/assets/includes/forms/new_event.php" method="post">
                        <center>
                            <div class="input-group input-group-lg">
                                <input type="text" placeholder="Titel.." class="form-control" name="title"/>
                            </div>
                            <div class="input-group">
                                <textarea name="description">
                                    Text
                                </textarea>
                                <input type="submit" name="submit" value="Speichern" class="btn btn-lg btn-success" style="float: right"/>
                            </div>
                        </center>
                    </form>
                </div>
            </div>
<?php require_once "/var/www/html/admin/assets/includes/bottom.inc.php"; ?>
