<?php require_once "/var/www/html/admin/assets/includes/top.inc.php"; ?>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Neuen Changelog hinzuf&uuml;gen</h1>
                    <form action="/assets/includes/forms/new_changelog.php" method="post">
                        <center>
                            <div class="input-group">
                                <textarea name="content">
                                    <ul>
                                        <li>Text</li>
                                    </ul>
                                </textarea>
                                <select name="prefix">
                                    <option value="common" selected>Allgemein</option>
                                    <option value="minecraft">Minecraft-Server</option>
                                    <option value="ts3">TeamSpeak3-Server</option>
                                    <option value="website">Website</option>
                                    <option value="hg">Hunger Games</option>
                                    <option value="dm">Deathmatch</option>
                                    <option value="infw">Infection Wars</option>
                                    <option value="spvp">SoupPvP</option>
                                </select>
                                <input type="submit" name="submit" value="Speichern" class="btn btn-lg btn-danger" style="float: right"/>
                            </div>
                        </center>
                    </form>
                </div>
            </div>
<?php require_once "/var/www/html/admin/assets/includes/bottom.inc.php"; ?>
