<?php
	if(!isset($_GET['sid'])) {
		header('Location: /servers');
		return;
	}

	$sid = $_GET['sid'];

	require_once "/home/apache2/admin/assets/includes/mysql.inc.php";

	$q = mysql_query("SELECT * FROM server WHERE sid=" . $sid);

	$noserver = false;
	if(mysql_num_rows($q) != 1) {
		$noserver = true;
	}

	if(!$noserver) {
		while($row = mysql_fetch_array($q)) {
			$directory = $row['directory'];
			$type = $row['type'];
		}

		if($type == "proxy") {
			$file = $directory . "proxy.log.0";
		}
		else if($type == "server") {
			$file = $directory . "logs/latest.log";
		}
		else {
			header('Location: /servers/view/?sid=' . $sid);
		}

		if(file_exists($file)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.basename($file));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			ob_clean();
			flush();
			readfile($file);
		}
	}
	mysql_close($link);
?>
