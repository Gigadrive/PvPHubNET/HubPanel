<?php
	if(!isset($_GET['sid'])) {
		header('Location: /servers');
		return;
	}

	$sid = $_GET['sid'];

	require_once "/home/apache2/admin/assets/includes/top.inc.php";

	$q = mysql_query("SELECT * FROM server WHERE sid=" . $sid);

	$noserver = false;
	if(mysql_num_rows($q) != 1) {
		$noserver = true;
	}

	if($noserver) {
?>
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Server</h1>
					<div class="row">
						<div class="col-lg-12">
							<i>Der Server mit der SID '<?php print $sid; ?>' konnte nicht gefunden werden!</i>
						</div>
					</div>
				</div>
			</div>
<?php
		require_once "/home/apache2/admin/assets/includes/bottom.inc.php";
	}
	else {
		while($row = mysql_fetch_array($q)) {
			$type = $row['type'];
			$name = $row['name'];
			$ip = $row['ip'];
			$port = $row['port'];
			$directory = $row['directory'];
		}

		require_once "/home/apache2/www/assets/includes/minetoweb.inc.php";
		require_once "/home/apache2/www/assets/includes/status.class.php";
		require_once "/home/apache2/www/assets/includes/online.inc.php";

		$status = new MinecraftServerStatus(1);

		$v = '1.7.*';
		$response = $status->getStatus($ip, $v, $port);

		if(!$response) {
			$mc = array(
				"status" => "offline"
			);
		}
		else {
			$mc = array(
				"status" => "online",
				"hostname" => $response['hostname'],
				"version" => $response['version'],
				"protocol" => $response['protocol'],
				"maxplayers" => $response['maxplayers'],
				"motd" => replacets3($response['motd']),
				"motd_raw" => replacets3($response['motd_raw']),
				"favicon" => $response['favicon'],
				"ping" => $response['ping'],
				"players" => $response['players'],
				"fpl" => $response['fpl']
			);
		}

		if($mc['status'] == "online") {
			$online = "<font color='#00FF00'>Ja</font>";
			$op = $mc['players'];
			$mp = $mc['maxplayers'];
			$protocol = $mc['protocol'];
			$version = $mc['version'];
			$motd = MineToWeb($mc['motd_raw']);
			$ping = $mc['ping'];
			$fpl = $mc['fpl'];
		}
		else {
			$online = "<font color='#FF0000'>Nein</font>";
			$op = "-1";
			$mp = "-1";
			$protocol = "-1";
			$version = "-1";
			$motd = "";
			$ping = "";
		}

		if($op != "-1" and $mp != "-1") {
			$sp = $op . "/" . $mp;
		}
		else {
			$sp = "";
		}

		if($protocol != "-1" and $version != "-1") {
			$vp = $version . " (" . $protocol . ")";
		}
		else {
			$vp = "";
		}
?>
			<div class="row">
                <div class="col-lg-12">
					<script type="text/javascript" src="/assets/includes/serverview.js"></script>
                    <h1 class="page-header">Server</h1>
					<ul class="nav nav-tabs" id="playerlist">
						<li class="active"><a href="#info" data-toggle="tab">Informationen</a></li>
						<li><a href="#lognconsole" data-toggle="tab" onclick="updateConsole('clog','cdiv','<?php print $directory; ?>','<?php print $type; ?>')">Log & Konsole</a></li>
						<li><a href="#filebrowser" data-toggle="tab">File Browser</a></li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane fade in active" id="info">
							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-heading">Server Informationen</div>
										<div class="panel-body">
											<div class="table-responsive">
												<table class="table table-striped">
													<tbody>
														<tr>
															<td><b>SID:</b></td>
															<td><?php print $sid; ?></td>
														</tr>
														<tr>
															<td><b>Name:</b></td>
															<td><?php print $name; ?></td>
														</tr>
														<tr>
															<td><b>Typ:</b></td>
															<td><?php print $type; ?></td>
														</tr>
														<tr>
															<td><b>IP & Port:</b></td>
															<td><?php print $ip . ":" . $port; ?></td>
														</tr>
														<tr>
															<td><b>Server Verzeichnis:</b></td>
															<td><?php print $directory ?></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">Online Informationen</div>
										<div class="panel-body">
											<div class="table-responsive">
												<table class="table table-striped">
													<tbody>
														<tr>
															<td><b>Online:</b></td>
															<td><?php print $online; ?></td>
														</tr>
														<tr>
															<td><b>Ping Version:</b></td>
															<td><?php print $v; ?></td>
														</tr>
														<tr>
															<td><b>Spieler:</b></td>
															<td><?php print $sp; ?></td>
														</tr>
														<tr>
															<td><b>Server Version & Protokoll:</b></td>
															<td><?php print $vp; ?></td>
														</tr>
														<tr>
															<td><b>MOTD:</b></td>
															<td><?php print $motd; ?></td>
														</tr>
														<tr>
															<td><b>Ping:</b></td>
															<td><?php print $ping; ?></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="tab-pane fade in" id="lognconsole">
							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-heading">Log & Konsole
											<div style="float: right;">
												<button type="button" onclick="alert('Funktion folgt')" class="btn btn-danger" style="position: relative; top: -7px;">Server stoppen</button>
												<button type="button" onclick="scrollDown('clog')" class="btn btn-info" style="position: relative; top: -7px;">Nach unten</button>
												<button class="btn btn-info" id="rbtn" data-loading-text="Refreshing..." style="position: relative; top: -7px;" onclick="updateConsole('clog','cdiv','<?php print $directory; ?>', '<?php print $type; ?>', '#rbtn')">Refresh</button>
												<button <?php if($type != "proxy" and $type != "server") { print 'disabled="true" '; } ?>onclick="window.location.href='/servers/view/logdl/?sid=<?php print $sid ?>'" type="button" class="btn btn-info" style="position: relative; top: -7px;">Download Log<?php if($type == "proxy") { print " (" . round(filesize($directory . "proxy.log.0") / 1024 / 1024, 2) . "MB)"; } else if($type == "server") { print " (" . round(filesize($directory . "logs/latest.log") / 1024 / 1024, 2) . "MB)"; }?></button>
											</div>
										</div>
										<div id="cdiv" class="panel-body">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="tab-pane fade in" id="filebrowser">
							File Browser
						</div>
					</div>
				</div>
			</div>
<?php
		require_once "/home/apache2/admin/assets/includes/bottom.inc.php";
	}
?>
